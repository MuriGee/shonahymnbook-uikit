//
//  ViewController.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 24/09/2019.
//  Copyright © 2019 MuriGee. All rights reserved.
//

import UIKit
import Firebase

class PageViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var db: Firestore!
    
    var PageArray = [PageItem]()

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        setUpVariables()
        loadData()
        //writeData()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Variable Setup
    func setUpVariables() {
        db = Firestore.firestore()
    }
    
    // MARK: - Load Data
    func loadData() {
        // TODO: - page order
        db.collection("Pages").order(by: "pageNo", descending: false).addSnapshotListener() { (querySnapshot, err) in
            self.PageArray = querySnapshot!.documents.compactMap({PageItem(dictionary: $0.data())})
            self.collectionView.reloadData()
        }
    }
    
    // TODO: - write data -- TO BE DELETED.
    func writeData() {
        var sum = 30

        for _ in 1...10{
            sum+=1
            db.collection("Hymns").document("hymn_\(sum)").setData(["hymnNo" : sum, "title": "test", "verses": ["test1", "test2", "test3", "test4", "test5"]])
            print("Value: \(sum)")
            print("Break")
        }
    }
}

extension PageViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PageCollectionViewCell
        let page = PageArray[indexPath.row]
        
        cell.pageNumber.text = "\(page.pageNo)"
        let pageImageUrl = page.pageImageUrl
        let url = URL(string: pageImageUrl)
        URLSession.shared.dataTask(with: url!, completionHandler: { data, response, error in
            if error != nil {
                print(error!)
                return
            }
            DispatchQueue.main.async {
                cell.pageImage?.image = UIImage(data: data!)
            }
        }).resume()
        return cell
    }
}

extension PageViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "displayPage", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = collectionView.indexPathsForSelectedItems?.first {
            let destinationVC = segue.destination as! PageDisplayViewController
            let documentID = "page_\(PageArray[indexPath.row].pageNo)"
            destinationVC.documentID = documentID
        }
    }
}
