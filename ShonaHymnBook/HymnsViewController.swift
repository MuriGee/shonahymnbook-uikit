//
//  HymnsViewController.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 18/10/2019.
//  Copyright © 2019 MuriGee. All rights reserved.
//

import UIKit
import Firebase

class HymnsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var db: Firestore!
    let searchController = UISearchController(searchResultsController: nil)
    
    var HymnArray = [HymnItem]()
    var filteredHymns = [HymnItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        setupVariables()
        loadData()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Variable Setup
    func setupVariables() {
        db = Firestore.firestore()
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Hymns"
        navigationItem.searchController = searchController
    }
    
    // MARK :- Load Data
    func loadData() {
        db.collection("Hymns").order(by: "hymnNo", descending: false).getDocuments() { (querySnapshot, err) in
            self.HymnArray = querySnapshot!.documents.compactMap({HymnItem(dictionary: $0.data())})
            self.tableView.reloadData()
        }
    }
    
    var isFiltering: Bool {
        return searchController.isActive && !searchController.searchBar.text!.isEmpty
    }
    
    private func filterForHymns(for searchText: String) {
      filteredHymns = HymnArray.filter { hymn in
        return hymn.title.lowercased().contains(searchText.lowercased()) || hymn.chorus.lowercased().contains(searchText.lowercased()) /*|| hymn.verses.contains(searchText.lowercased())*/
      }
      tableView.reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension HymnsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && !searchController.searchBar.text!.isEmpty {
            return filteredHymns.count
        }
        return HymnArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        let hymn: HymnItem
        if isFiltering {
            hymn = filteredHymns[indexPath.row]
        } else {
            hymn = HymnArray[indexPath.row]
        }
        if hymn.oldHymnNo == 0 {
            cell.textLabel?.text = "\(hymn.hymnNo) (-) - \(hymn.title)"
        } else {
            cell.textLabel?.text = "\(hymn.hymnNo) (\(hymn.oldHymnNo)) - \(hymn.title)"
        }
        return cell
    }
}

extension HymnsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "displayHymn", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathsForSelectedRows?.first {
            let destinationVC = segue.destination as! HymnDisplayViewController
            let documentID = HymnArray[indexPath.row]
            destinationVC.documentID = documentID
        }
    }
}

extension HymnsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterForHymns(for: searchBar.text!)
    }
}
