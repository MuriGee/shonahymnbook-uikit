//
//  PageCollectionViewCell.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 27/09/2019.
//  Copyright © 2019 MuriGee. All rights reserved.
//

import UIKit

class PageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var pageImage: UIImageView!
    @IBOutlet weak var pageNumber: UILabel!
    
}
