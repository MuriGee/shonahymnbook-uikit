//
//  PageDisplayViewController.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 17/10/2019.
//  Copyright © 2019 MuriGee. All rights reserved.
//

import UIKit
import Firebase

class PageDisplayViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var PageLabel: UILabel!
    var db: Firestore!
    var documentID: String!
    var pageNum: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupVariables()
        loadData()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Variable Setup
    func setupVariables() {
        db = Firestore.firestore()
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
            
        leftSwipe.direction = .left
        rightSwipe.direction = .right

        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
    }
    
    // MARK: - Load Data
    func loadData() {
        db.collection("Pages").document(documentID).getDocument { (document, error) in
            self.pageNum = (document?.get("pageNo") as! Int)
            let imageUrl = document?.get("pageImageUrl") as! String
            let url = URL(string: imageUrl)
            URLSession.shared.dataTask(with: url!, completionHandler: { data, response, error in
                if error != nil {
                    print(error!)
                    return
                }
                DispatchQueue.main.async {
                    self.imageView.image = UIImage(data: data!)
                }
            }).resume()
            self.PageLabel.text = "\(self.pageNum!)"
        }
    }
    
    // MARK:- Swipe Recogniser
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .left) {
            // TODO: if to be changed to represent last page in database (when added).
            if pageNum == 199 {
                return
            } else {
                documentID = "page_\(pageNum+1)"
                loadData()
                print("Swiped Left")
                print(documentID!)
            }
            
        }
        
        if (sender.direction == .right) {
            if pageNum == 1 {
                return
            } else {
                documentID = "page_\(pageNum-1)"
                loadData()
                print("Swiped Right")
                print(pageNum-1)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
