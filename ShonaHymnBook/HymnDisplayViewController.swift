//
//  HymnDisplayViewController.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 21/10/2019.
//  Copyright © 2019 MuriGee. All rights reserved.
//

import UIKit
import Firebase

class HymnDisplayViewController: UIViewController {

    @IBOutlet weak var hymnLabel: UILabel!
    @IBOutlet weak var hymnDisplayTextView: UITextView!
    var db: Firestore!
    var documentID: HymnItem?
    var hymnTitle: String!
    var hymnNum: Int!
    var verseArray: [String]!
    var chorus: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpVariables()
        loadData()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Variable Setup
    func setUpVariables() {
        db = Firestore.firestore()
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
            
        leftSwipe.direction = .left
        rightSwipe.direction = .right

        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
    }
    
    // MARK: - Load Data
    func loadData() {
        self.hymnNum = documentID?.hymnNo
        self.hymnTitle = documentID?.title
        self.verseArray = (documentID?.verses)
        self.hymnLabel.text = "\(self.hymnNum!) - \(self.hymnTitle!)"
        if (documentID?.chorus) != "" {
            print("success")
            self.chorus = (documentID?.chorus)
            let chorusText = self.chorus.replacingOccurrences(of: "\\n", with: "\n")
            var text = ""
            var verseNo = 1
            for i in self.verseArray {
                print("Start")
                let verseText = i.replacingOccurrences(of: "\\n", with: "\n")
                print(verseText)
                text += "Verse \(verseNo) \n" + verseText + "\n\nChorus\n\(chorusText)\n\n"
                verseNo+=1
            }
            self.hymnDisplayTextView.text = text
            
        } else {
            var text = ""
            var verseNo = 1
            for i in self.verseArray {
                print("Start")
                let verseText = i.replacingOccurrences(of: "\\n", with: "\n")
                print(verseText)
                text += "Verse \(verseNo) \n" + verseText + "\n\n"
                verseNo+=1
            }
            self.hymnDisplayTextView.text = text
        }
    }
    
    // MARK:- Swipe Recogniser
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        /*
        if (sender.direction == .left) {
            // TODO: if to be changed to represent last hymn in database (when added).
            if hymnNum == 10 {
                return
            } else {
                documentID = "hymn_\(hymnNum+1)"
                loadData()
                print("Swiped Left")
                print(documentID!)
            }
        }
        
        if (sender.direction == .right) {
            if hymnNum == 1 {
                return
            } else {
                documentID = "hymn_\(hymnNum-1)"
                loadData()
                print("Swiped Right")
                print(hymnNum-1)
            }
        }
    */
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
