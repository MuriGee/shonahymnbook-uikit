//
//  HymnsItem.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 18/10/2019.
//  Copyright © 2019 MuriGee. All rights reserved.
//

import Foundation
import Firebase

struct HymnItem {
    let hymnNo: Int
    let oldHymnNo: Int
    let title: String
    let verses: [String]
    let chorus: String
    
    var dictionary: [String:Any] {
        return ["hymnNo": hymnNo,
                "oldHymnNo": oldHymnNo,
                "title": title,
                "verses": verses,
                "chorus": chorus]
    }
}

extension HymnItem : DocumentSerializeable {
    init?(dictionary: [String : Any]) {
        guard let title = dictionary["title"] as? String,
            let verses = dictionary["verses"] as? [String],
            let hymnNo = dictionary["hymnNo"] as? Int
        else { return nil }
        var hymn: Int = 0
        if let found = dictionary["oldHymnNo"] as? Int {
            hymn = found
        }
        var hymnChorus: String = ""
        if let found = dictionary["chorus"] as? String {
            hymnChorus = found
        }
        self.init(hymnNo: hymnNo, oldHymnNo: hymn, title: title, verses: verses, chorus: hymnChorus)
    }
}
