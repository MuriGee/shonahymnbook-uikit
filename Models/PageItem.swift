//
//  PageItem.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 30/09/2019.
//  Copyright © 2019 MuriGee. All rights reserved.
//

import Foundation
import Firebase

protocol DocumentSerializeable {
    init?(dictionary:[String:Any])
}

struct PageItem {
    let pageNo: Int
    let pageImageUrl: String
    
    var dictionary: [String:Any] {
        return ["pageImageUrl": pageImageUrl,
                "pageNo" : pageNo
        ]
    }
}

extension PageItem : DocumentSerializeable {
    init?(dictionary: [String : Any]) {
        guard let pageImageUrl = dictionary["pageImageUrl"] as? String,
            let pageNo = dictionary["pageNo"] as? Int
            else { return nil }
        
        self.init(pageNo: pageNo, pageImageUrl: pageImageUrl)
    }
}
